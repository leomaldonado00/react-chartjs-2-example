import React, { useState } from "react";
import {
  Line,
  Bar,
  HorizontalBar,
  Doughnut,
  defaults,
  Radar,
  Pie
} from "react-chartjs-2";
import "./App.css";

// defaults.global.maintainAspectRatio = false; // maintainAspectRatio option can be specified globally and be applied to every chart by importing the defaults object from react-chartjs-2 and changing the global values.

// EXPRESIONES REGULARES
// var regex = /\d{2,2}\/d{2,2}\/d{4,4}/g;
var regex = /[\d{2}\/\d{2}\/\d{4}]/;
var tags = /[^<>]+|<(\/?)([A-Za-z]+)([^<>]*)>/g;
const strg = "naci el05/04/1982 en Donostia.";

const result = strg.match(regex);
console.log("result", result);

function App() {
  const [formLogin, setformLogin] = useState({
    nombre: "",
    apellido: "",
    email: "",
    phone: "",
    password: "",
    date: "",
    hour: "",
    ip: "",
    url: "",
    pizza: false,
    pasta: false,
    arroz: false,
    ciudad: "",

    Enombre: "",
    Eapellido: "",
    Eemail: "",
    Ephone: "",
    Epassword: "",
    Edate: "",
    Ehour: "",
    Eip: "",
    Eurl: "",
    Ecomidas: "",
    Eciudad: "",

    Eflag: false
  });

  // VALIDACION **********************************
  const escrib = e => {
    //console.log(e.target.name, e.target.value, e.target.checked, e.target.type);
    if (e.target.type === "checkbox" /*|| e.target.type === "radio"*/) {
      setformLogin({
        ...formLogin,
        [e.target.name]: e.target.checked,
        [`E${e.target.value}`]: ""
      });
    } else {
      setformLogin({
        ...formLogin,
        [e.target.name]: e.target.value,
        [`E${e.target.name}`]: ""
      });
    }
    /*if (formLogin.email !== 0) {
      setformLogin({ ...formLogin, Eemail: "" });
    }*/
    //console.log(formLogin);
  };

  const validate = dataLogin => {
    let errores = {};
    if (
      !dataLogin.email ||
      !dataLogin.password ||
      !dataLogin.date ||
      !dataLogin.hour ||
      !dataLogin.ip ||
      !dataLogin.url ||
      !dataLogin.nombre ||
      !dataLogin.apellido ||
      !dataLogin.phone ||
      !dataLogin.ciudad ||
      (!dataLogin.arroz && !dataLogin.pasta && !dataLogin.pizza)
    ) {
      if (dataLogin.email.length === 0) {
        errores = { ...errores, Eemail: "El email es requerido", Eflag: true };
      }
      if (dataLogin.password.length === 0) {
        errores = {
          ...errores,
          Epassword: "El password es requerido",
          Eflag: true
        };
      }
      if (dataLogin.date.length === 0) {
        errores = { ...errores, Edate: "El date es requerido", Eflag: true };
      }
      if (dataLogin.hour.length === 0) {
        errores = { ...errores, Ehour: "El hour es requerido", Eflag: true };
      }
      if (dataLogin.ip.length === 0) {
        errores = { ...errores, Eip: "El ip es requerido", Eflag: true };
      }
      if (dataLogin.url.length === 0) {
        errores = { ...errores, Eurl: "El url es requerido", Eflag: true };
      }
      if (dataLogin.nombre.length === 0) {
        errores = {
          ...errores,
          Enombre: "El nombre es requerido",
          Eflag: true
        };
      }
      if (dataLogin.apellido.length === 0) {
        errores = {
          ...errores,
          Eapellido: "El apellido es requerido",
          Eflag: true
        };
      }
      if (dataLogin.phone.length === 0) {
        errores = { ...errores, Ephone: "El phone es requerido", Eflag: true };
      }
      if (dataLogin.ciudad.length === 0) {
        errores = {
          ...errores,
          Eciudad: "Es requerida una ciudad de residencia!!!",
          Eflag: true
        };
      }
      if (!dataLogin.arroz && !dataLogin.pasta && !dataLogin.pizza) {
        errores = {
          ...errores,
          Ecomidas: "Es requerida al menos 1 comida!!!"
        };
      }
      console.log("errores:", errores);
      setformLogin({ ...dataLogin, ...errores });
      return false;
    } /*else {
      setdataLogin({ ...dataLogin, Eflag: false });
    }*/
    if (
      dataLogin.email.length !== 0 &&
      !/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(
        dataLogin.email
      )
    ) {
      errores = {
        ...errores,
        Eemail: "La dirección de email es incorrecta.",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.password.length !== 0 &&
      !/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/.test(dataLogin.password)
    ) {
      errores = {
        ...errores,
        Epassword:
          "Contraseña debe tener al menos un numero y una mayuscula, entre 8 y 16 caracteres(sin simbols)",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.date.length !== 0 &&
      !/^(?:3[01]|[12][0-9]|0?[1-9])([\-/.])(0?[1-9]|1[1-2])\1\d{4}$/.test(
        dataLogin.date
      )
    ) {
      errores = {
        ...errores,
        Edate: "fecha debe ser dataato dd/mm/aaaa.",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.hour.length !== 0 &&
      !/^(?:0?[1-9]|1[0-2]):[0-5][0-9]\s?(?:[aApP](\.?)[mM]\1)?$/.test(
        dataLogin.hour
      )
    ) {
      errores = {
        ...errores,
        Ehour: "Hora debe ser dataato hh:mmXM",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.ip.length !== 0 &&
      !/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
        dataLogin.ip
      )
    ) {
      errores = {
        ...errores,
        Eip: "La dirección ip es incorrecta.",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.url.length !== 0 &&
      !/^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$/.test(dataLogin.url)
    ) {
      errores = {
        ...errores,
        Eurl: "La dirección de url es incorrecta.",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.phone.length !== 0 &&
      !/^(\(\+?\d{2,3}\)[\*|\s|\-|\.]?(([\d][\*|\s|\-|\.]?){6})(([\d][\s|\-|\.]?){2})?|(\+?[\d][\s|\-|\.]?){8}(([\d][\s|\-|\.]?){2}(([\d][\s|\-|\.]?){2})?)?)$/.test(
        dataLogin.phone
      )
    ) {
      errores = {
        ...errores,
        Ephone: "dataato de phone incorrecto",
        Eflag: true
      };
      setformLogin({ ...dataLogin, ...errores });
      return false;
    }

    setformLogin({ ...dataLogin, Eflag: false });
    return true;

    //}
  };

  const validate_0 = dataLogin => {
    const siono = validate(dataLogin);
    if (siono) {
      alert("ENVIADO");
    } else {
      alert("NO ENVIADO");
    }
    console.log(formLogin);
  };
  const showState = () => {
    console.log("STATE", formLogin);
  };
  // VALIDACION **********************************

  const data = {
    labels: [
      //  which is the array containing our labels, X axis values.
      "10/04/2018",
      "10/05/2018",
      "10/06/2018",
      "10/07/2018",
      "10/08/2018",
      "10/09/2018",
      "10/10/2018",
      "10/11/2018",
      "10/12/2018",
      "10/13/2018",
      "10/14/2018",
      "10/15/2018"
    ],
    datasets: [
      // which is an array of Dataset objects containing the properties of a line such as the Y axis data, line color, etc. You can find all the Dataset properties here.
      {
        label: "Temperature",
        data: [22, 19, 27, 23, 22, 24, 17, 25, 23, 24, 20, 19], // Y axis values corresponding to labels:[.x..]
        fill: false, // Don't fill area under the line
        borderColor: "#0456", // Line color
        backgroundColor: [
          "#0456",
          "red",
          "blue",
          "violet",
          "lime",
          "black",
          "gray",
          "yellow",
          "orange",
          "brown",
          "aqua",
          "green"
        ], // backgoround
        hoverBackgroundColor: "purple"
      }
    ]
  };

  const options = {
    maintainAspectRatio: false // Don't maintain w/h ratio
  };

  return (
    <div>
      <header className="App-header">
        <h1>Linear chart --> Chart.js</h1>
      </header>
      <div className="canvas-container">
        <Line data={data} options={options} />
      </div>
      <div>
        <Bar
          data={data}
          width={100}
          height={500}
          options={{ maintainAspectRatio: false }}
        />
      </div>
      <div>
        <HorizontalBar
          data={data}
          width={100}
          height={500}
          options={{ maintainAspectRatio: false }}
        />
      </div>
      <div>
        <Radar data={data} options={options} width={100} height={500} />
      </div>
      <div>
        <Doughnut data={data} options={options} width={100} height={500} />
      </div>
      <div>
        <Pie data={data} options={options} width={100} height={500} />
      </div>
      <h1>h</h1>
      <h1>h</h1>
      <h1>h</h1>
      <h1>h</h1>
      <h1>h</h1>
      <h1>h</h1>
      <h1>h</h1>
      <hr />
      {/*  // VALIDACION ***********************************/}
      <h1>FORMULARIO VALIDACION RegEx</h1>
      {formLogin.Enombre.length !== 0 /*|| formLogin.nombre.length !== 0*/ ? (
        <p>{formLogin.Enombre}</p>
      ) : null}
      <input
        type="text"
        name="nombre"
        placeholder="nombre"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Eapellido.length !==
      0 /*|| formLogin.apellido.length !== 0*/ ? (
        <p>{formLogin.Eapellido}</p>
      ) : null}
      <input
        type="text"
        name="apellido"
        placeholder="apellido"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Eemail.length !== 0 /*|| formLogin.email.length !== 0*/ ? (
        <p>{formLogin.Eemail}</p>
      ) : null}
      <input
        type="text"
        name="email"
        placeholder="email"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Ephone.length !== 0 /*|| formLogin.phone.length !== 0*/ ? (
        <p>{formLogin.Ephone}</p>
      ) : null}
      <input
        type="text"
        name="phone"
        placeholder="phone number"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Edate.length !== 0 /*|| formLogin.date.length !== 0*/ ? (
        <p>{formLogin.Edate}</p>
      ) : null}
      <input
        type="text"
        name="date"
        placeholder="date dd/mm/aaaa"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Ehour.length !== 0 /*|| formLogin.hour.length !== 0*/ ? (
        <p>{formLogin.Ehour}</p>
      ) : null}
      <input
        type="text"
        name="hour"
        placeholder="hour hh:mmXM"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Eip.length !== 0 /*|| formLogin.ip.length !== 0*/ ? (
        <p>{formLogin.Eip}</p>
      ) : null}
      <input
        type="text"
        name="ip"
        placeholder="IP Address"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Eurl.length !== 0 /*|| formLogin.url.length !== 0*/ ? (
        <p>{formLogin.Eurl}</p>
      ) : null}
      <input
        type="text"
        name="url"
        placeholder="URL enlace"
        onChange={e => escrib(e)}
      ></input>
      {formLogin.Epassword.length !==
      0 /*|| formLogin.password.length !== 0*/ ? (
        <p>{formLogin.Epassword}</p>
      ) : null}
      <input
        type="password"
        name="password"
        placeholder="password"
        onChange={e => escrib(e)}
      ></input>{" "}
      <hr />
      <p>Selecciona las comidas que te gusten:</p>
      {formLogin.Ecomidas.length !==
      0 /*|| formLogin.password.length !== 0*/ ? (
        <p>{formLogin.Ecomidas}</p>
      ) : null}
      <label>
        <input
          type="checkbox"
          value="comidas"
          name="arroz"
          onChange={e => escrib(e)}
        />
        Arroz
      </label>
      <label>
        <input
          type="checkbox"
          value="comidas"
          name="pasta"
          onChange={e => escrib(e)}
        />
        Pasta
      </label>
      <label>
        <input
          type="checkbox"
          value="comidas"
          name="pizza"
          onChange={e => escrib(e)}
        />
        Pizza
      </label>
      <hr />
      <p>Selecciona UNA ciudad de residencia:</p>
      {formLogin.Eciudad.length !== 0 /*|| formLogin.password.length !== 0*/ ? (
        <p>{formLogin.Eciudad}</p>
      ) : null}
      <label>
        <input
          type="radio"
          value="Cúa"
          name="ciudad"
          onChange={e => escrib(e)}
        />
        Cúa
      </label>
      <label>
        <input
          type="radio"
          value="Charallave"
          name="ciudad"
          onChange={e => escrib(e)}
        />
        Charallave
      </label>
      <label>
        <input
          type="radio"
          value="Caracas"
          name="ciudad"
          onChange={e => escrib(e)}
        />
        Caracas
      </label>
      <hr />
      <label>
        Deje un mensaje (opcional):
        <textarea />
      </label>{" "}
      <hr />
      <button onClick={() => validate_0(formLogin)}>LOGIN</button>
      <button onClick={() => showState()}>STATE</button>
    </div>
  );
}

export default App;
